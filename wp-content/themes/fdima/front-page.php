<?php get_header(); ?>

<h1 style="display: none;"><?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?></h1>

<?php
	global $product;

	//field
	$home_banner_slider 				= get_field('home_banner_slider');

	$home_ads_image_banner_left 		= get_field('home_ads_image_banner_left');
	$home_ads_image_banner_left_link 	= get_field('home_ads_image_banner_left_link');
	$home_ads_image_banner_right 		= get_field('home_ads_image_banner_right');
	$home_ads_image_banner_right_link 	= get_field('home_ads_image_banner_right_link');

	$home_product_feature_title_section = get_field('home_product_feature_title_section');
	$home_product_feature_select 		= get_field('home_product_feature_select');

	$home_product_tab_content 			= get_field('home_product_tab_content');
	$home_product_tab_icon 				= get_field('home_product_tab_icon');

	$home_ads_image 					= get_field('home_ads_image');
	$home_ads_image_link 				= get_field('home_ads_image_link');

	$home_news_title_section 			= get_field('home_news_title_section');
	$home_news_select 					= get_field('home_news_select');
?>

<section class="vk-content">

<div class="vk-home__banner">
    <div class="container">
        <div class="row">

            <div class="col-lg-3">
				<?php
				    if(function_exists('wp_nav_menu')){
				        $args = array(
				            'theme_location' 	=> 	'menu_cat_home',
				            'container_class'	=>	'menu-danh-muc-trang-chu-container',
				            'menu_class'		=>	'vk-menu__nav-list'
				        );
				        wp_nav_menu( $args );
				    }
				?>
            </div>

            <?php if(!empty( $home_banner_slider )) { ?>
            <div class="col-lg-9">
                <div class="vk-banner__slider slick-slider" data-slider="banner">

					<?php
					    foreach ($home_banner_slider as $foreach_kq) {

					    $post_image = $foreach_kq;
					?>
                    <div class="_item">
                        <div class="vk-img">
                            <img src="<?php echo $post_image; ?>" alt="banner">
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
            <?php } ?>

        </div>
    </div>
</div>


<div class="vk-home__ads pt-20">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
            	<a title href="<?php echo $home_ads_image_banner_left_link; ?>" class="vk-img">
                    <img src="<?php echo $home_ads_image_banner_left; ?>" class="_img" alt="img_ads">
                </a>
            </div>
            <div class="col-lg-6">
            	<a title href="<?php echo $home_ads_image_banner_right_link; ?>" class="vk-img">
                    <img src="<?php echo $home_ads_image_banner_right; ?>" class="_img" alt="img_ads">
                </a>
            </div>
        </div>
    </div>
</div>


<?php if(!empty( $home_product_feature_select )) { ?>
<div class="vk-home__shop pt-40">
    <div class="container">
        <h2 class="vk-home__title"><?php echo $home_product_feature_title_section; ?></h2>
        <div class="vk-shop__list row2 slick-slider" data-slider="relate">
            
		<?php
		    foreach ($home_product_feature_select as $foreach_kq) {

		    $post_id 		= $foreach_kq->ID;
			$post_title 	= get_the_title($post_id);
			$post_date 		= get_the_date('d m Y',$post_id);
			$post_link 		= get_permalink($post_id);
			$post_image 	= getPostImage($post_id,"p-product");
			$post_excerpt 	= cut_string(get_the_excerpt($post_id),200,'...');

            // wc
            $money =  wc_get_product( $post_id );
            $old_price = (float)$money->get_regular_price();
            $price = (float)$money->get_sale_price();

            // product;
            $product = wc_get_product( $post_id );
            foreach ($product->attributes as $key => $foreach_kq) {

            }
            $product_attributes_name = $key;

            $product_sku = $product->sku;
            $product_brand = array_shift( wc_get_product_terms( $post_id, $product_attributes_name, array( 'fields' => 'names' ) ) );

            // //gallery
            $product = new WC_product($post_id);
            $single_product_gallery = $product->get_gallery_image_ids();

            // field
            $s_p_skill_content = wpautop( get_post_meta( $post_id, 's_p_skill_content', true ) );
		?>

            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-self _item hover-product" data-productid="<?php echo $post_id; ?>">
                <div class="vk-shop-item">

                	<?php if( !empty($price) ) { ?>
                    <div class="vk-shop-item__sale">
                        - <?php echo show_sale($old_price, $price); ?>
                    </div>
                    <?php } ?>

                    <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="vk-shop-item__img">
                        <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
                    </a>
                    <div class="vk-shop-item__brief">
                        <h3 class="vk-shop-item__title">
                        	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" data-truncate-lines="1">
                        		<?php echo $post_title; ?>
                    		</a>
                    	</h3>

                        <div class="vk-shop-item__price">
                            <?php echo show_price_old_price($old_price,$price,get_woocommerce_currency_symbol()); ?>
                        </div>

                        <div class="vk-shop-item__rate woocommerce">
			                <?php get_template_part("woocommerce/single-product/rating"); ?>
                        </div>
                        <div class="vk-shop-item__hover">
                            <div class="vk-shop-item__button">
                                <a title href="javascript:void(0)" class="vk-shop-item__btn woocommerce">
                                    <?php if($old_price > 0){ ?>
	                                    <form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $post_link ) ); ?>" method="post" enctype='multipart/form-data'>
	                                        <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $post_id ); ?>" class="single_add_to_cart_button button alt">
	                                            <i class="ti-shopping-cart"></i>
	                                        </button>
	                                    </form>
                                    <?php } else { ?>
                                        <i class="ti-shopping-cart"></i>
                                    <?php } ?>
                                </a>
                                <a title href="#quickview_2" data-toggle="modal" class="vk-shop-item__btn">Xem thêm</a>
                                <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>

        </div>
    </div>
</div>
<?php } ?>


<?php if(!empty( $home_product_tab_content )) { ?>
<?php
	$j = 1;
    foreach ($home_product_tab_content as $foreach_kq) {
    	$product_cat_id 		= $foreach_kq["product_cat"];
    	$ads_image_left 		= $foreach_kq["ads_image_left"];
    	$ads_image_left_link 	= $foreach_kq["ads_image_left_link"];
    	$ads_image_right 		= $foreach_kq["ads_image_right"];
    	$ads_image_right_link 	= $foreach_kq["ads_image_right_link"];

		$term_id 		= $product_cat_id;
		$term_desc 		= cut_string( $foreach_kq->description ,300,'...');
		$taxonomy_slug 	= $foreach_kq->taxonomy;
		$term_name 		= get_term( $term_id, $taxonomy_slug )->name;
		$term_link 		= get_term_link(get_term( $term_id ));
		$taxonomy_name 	= 'product_cat';

	    $term_childs = get_term_children( $term_id, $taxonomy_name );
	    $count = count($term_childs);

		// nếu item > 5 mới play slider
		$k = 0;
		$query = query_post_by_taxonomy_paged('product', $taxonomy_name, $term_id, 12);
		$post_count = $query->post_count;

?>
	<div class="vk-home__shop pt-40">
	    <div class="container">
	        <div class="vk-home__title-box vk-home__title-box--style-<?php echo $j; ?>">
	            <div class="_left">
	                <img src="<?php echo $home_product_tab_icon; ?>" alt="icon">
	                <span><?php echo $term_name; ?></span>
	            </div>
	            <div class="_right">
	                <ul>

	                    <?php
	                        if($count > 0) {
	                            echo '<ul class="_list">';
	                            foreach ($term_childs as $foreach_kq) {
	                                $term_link = get_term_link(get_term($foreach_kq));
	                                $term_name = get_term($foreach_kq)->name;
	                    ?>
	                                <li>
	                                	<a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
	                                		<?php echo $term_name; ?>
                                		</a>
                                	</li>
	                    <?php
	                            }
	                            echo '</ul>';
	                        }
	                    ?>

	                </ul>
	            </div>
	        </div>
	        <div class="row pb-10">
	            <div class="col-lg-6">
	            	<a title href="<?php echo $ads_image_left_link; ?>" class="vk-img">
	                    <img src="<?php echo $ads_image_left; ?>" class="_img" alt="img_ads">
	                </a>
	            </div>
	            <div class="col-lg-6">
	            	<a title href="<?php echo $ads_image_right_link; ?>" class="vk-img">
	                    <img src="<?php echo $ads_image_right; ?>" class="_img" alt="img_ads">
	                </a>
	            </div>
	        </div>
	        <div class="vk-shop__list row <?php if($post_count>5){echo 'slick-slider';} ?>" <?php if($post_count>5){echo 'data-slider="relate"';} ?>>

				<?php
					// $k = 0;
					// $query = query_post_by_taxonomy_paged('product', $taxonomy_name, $term_id, 12);

					if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

                    $post_id 		= get_the_ID();
                    $post_title 	= get_the_title($post_id);
                    $post_content 	= wpautop(get_the_content($post_id));
                    $post_date 		= get_the_date('Y/m/d',$post_id);
                    $post_link 		= get_permalink($post_id);
                    $post_image 	= getPostImage($post_id,"p-product");
                    $post_excerpt 	= cut_string(get_the_excerpt($post_id),80,'...');

		            // wc
		            $money =  wc_get_product( $post_id );
		            $old_price = (float)$money->get_regular_price();
		            $price = (float)$money->get_sale_price();

		            // product;
		            $product = wc_get_product( $post_id );
		            foreach ($product->attributes as $key => $foreach_kq) {

		            }
		            $product_attributes_name = $key;

		            $product_sku = $product->sku;
		            $product_brand = array_shift( wc_get_product_terms( $post_id, $product_attributes_name, array( 'fields' => 'names' ) ) );

		            // //gallery
		            $product = new WC_product($post_id);
		            $single_product_gallery = $product->get_gallery_image_ids();

		            // field
		            $s_p_skill_content = wpautop( get_post_meta( $post_id, 's_p_skill_content', true ) );
				?>

		            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-self _item hover-product" data-productid="<?php echo $post_id; ?>">
		                <div class="vk-shop-item ">

		                	<?php if( !empty($price) ) { ?>
		                    <div class="vk-shop-item__sale">
		                        - <?php echo show_sale($old_price, $price); ?>
		                    </div>
		                    <?php } ?>

		                    <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="vk-shop-item__img">
		                        <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
		                    </a>
		                    <div class="vk-shop-item__brief">
		                        <h3 class="vk-shop-item__title">
		                        	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" data-truncate-lines="1">
		                        		<?php echo $post_title; ?>
		                    		</a>
		                    	</h3>

		                        <div class="vk-shop-item__price">
		                            <?php echo show_price_old_price($old_price,$price,get_woocommerce_currency_symbol()); ?>
		                        </div>

		                        <div class="vk-shop-item__rate woocommerce">
		                        	<?php get_template_part("woocommerce/single-product/rating"); ?>
		                        </div>
		                        <div class="vk-shop-item__hover">
		                            <div class="vk-shop-item__button">
		                                <a title href="javascript:void(0)" class="vk-shop-item__btn woocommerce">
                                            <?php if($old_price > 0){ ?>
			                                    <form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $post_link ) ); ?>" method="post" enctype='multipart/form-data'>
			                                        <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $post_id ); ?>" class="single_add_to_cart_button button alt">
			                                            <i class="ti-shopping-cart"></i>
			                                        </button>
			                                    </form>
                                            <?php } else { ?>
                                                <i class="ti-shopping-cart"></i>
                                            <?php } ?>
		                                </a>
		                                <a title href="#quickview_2" data-toggle="modal" class="vk-shop-item__btn">Xem thêm</a>
		                                <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>

	            <?php $k++; endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

	        </div>
	    </div>
	</div>
<?php $j++; } ?>
<?php } ?>


<div class="pt-10">
    <div class="container">
        <a title href="<?php echo $home_ads_image_link; ?>" class="vk-img">
            <img src="<?php echo $home_ads_image; ?>" alt="img_ads" />
        </a>
    </div>
</div>


<?php if(!empty( $home_news_select )) { ?>
<div class="vk-home__blog pt-40">
    <div class="container">
        <h2 class="vk-home__title"><?php echo $home_news_title_section; ?></h2>
        <div class="vk-blog__list row">

			<?php
			    foreach ($home_news_select as $foreach_kq) {

			    $post_id 		= $foreach_kq->ID;
                $post_title 	= get_the_title($post_id);
                $post_content 	= wpautop(get_the_content($post_id));
                $post_date 		= 'Ngày '.get_the_date('d / m / Y',$post_id);
                $post_link 		= get_permalink($post_id);
                $post_image 	= getPostImage($post_id,"p-post");
                $post_excerpt 	= cut_string(get_the_excerpt($post_id),135,'...');
                $post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                $post_tag 		= get_the_tags($post_id);
			?>

                <div class="col-md-6 _item">
                    <div class="vk-blog-item ">
                        <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="vk-blog-item__img">
                            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
                        </a>
                        <div class="vk-blog-item__brief">
                            <h3 class="vk-blog-item__title">
                            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                            		<?php echo $post_title; ?>
                                </a>
                            </h3>
                            <div class="vk-blog-item__date"><?php echo $post_date; ?></div>
                            <div class="vk-blog-item__text">
                                <?php echo $post_excerpt; ?>
                            </div>
                        </div>
                    </div>
                </div>

			<?php } ?>

        </div>
    </div>
</div>
<?php } ?>

</section>

<?php get_footer(); ?>

