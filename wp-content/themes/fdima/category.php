<?php get_header(); ?>

<?php
	$category_info 	= get_category_by_slug( get_query_var( 'category_name' ) );
	$cat_id 		= $category_info->term_id;
	$cat_name 		= get_cat_name($cat_id);
	$cat_excerpt 	= wpautop(category_description($cat_id));
	$cat_link 		= esc_url(get_term_link($cat_id));

    //field
	$news_ads_image_left 	   = get_field('news_ads_image_left', 'option');
	$news_ads_image_left_link  = get_field('news_ads_image_left_link', 'option');

	$news_ads_image_right 	   = get_field('news_ads_image_right', 'option');
	$news_ads_image_right_link = get_field('news_ads_image_right_link', 'option');
?>

<section class="vk-content">
    
    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="vk-shop__before pt-10 pb-20">
        <div class="container">
            <div class="vk-shop__before-content">
                <div class="_wrapper">
                    <h1 class="vk-shop__heading"><?php echo $cat_name; ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="vk-blog__list row">

			<?php
				$query = query_post_by_category_paged($cat_id, 9);
				$max_num_pages = $query->max_num_pages;

				if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

                $post_id 		= get_the_ID();
                $post_title 	= get_the_title($post_id);
                $post_content 	= wpautop(get_the_content($post_id));
                $post_date 		= 'Ngày '.get_the_date('d / m / Y',$post_id);
                $post_link 		= get_permalink($post_id);
                $post_image 	= getPostImage($post_id,"p-post");
                $post_excerpt 	= cut_string(get_the_excerpt($post_id),135,'...');
                $post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                $post_tag 		= get_the_tags($post_id);
			?>

	            <div class="col-sm-6 col-md-3 col-lg-4 _item">
	                <div class="vk-blog-item ">
	                    <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="vk-blog-item__img">
	                        <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
	                    </a>
	                    <div class="vk-blog-item__brief">
	                        <h3 class="vk-blog-item__title">
	                        	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
	                        		<?php echo $post_title; ?>
	                        	</a>
	                        </h3>
	                        <div class="vk-blog-item__date"><?php echo $post_date; ?></div>
	                        <div class="vk-blog-item__text" data-truncate-lines="2">
	                            <?php echo $post_excerpt; ?>
	                        </div>
	                    </div>
	                </div>
	            </div>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>

        <nav class="vk-pagination">
        	<?php echo paginationCustom( $max_num_pages ); ?>
        </nav>

        <div class="row pt-40">
            <div class="col-lg-6">
                <a title href="<?php echo $news_ads_image_left_link; ?>" class="vk-img">
                    <img src="<?php echo $news_ads_image_left; ?>" alt="img_ads">
                </a>
            </div>
            <div class="col-lg-6">
                <a title href="<?php echo $news_ads_image_right_link; ?>" class="vk-img">
                    <img src="<?php echo $news_ads_image_right; ?>" alt="img_ads">
                </a>
            </div>
        </div>

    </div>
</section>

<?php get_footer(); ?>