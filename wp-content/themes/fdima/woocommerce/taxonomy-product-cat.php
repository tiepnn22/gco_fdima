<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

    $term_info = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $taxonomy_name = $term_info->taxonomy;

    $term_id        = $term_info->term_id;
    $term_name      = $term_info->name;
    $term_childs    = get_term_children( $term_id, $taxonomy_name );
    $count          = count($term_childs);

    // info attribute
    $array = wc_get_attribute_taxonomies();
    foreach ($array as $foreach_kq) {
        $attribute_id = $foreach_kq->attribute_id;
        $attribute_name = $foreach_kq->attribute_name;
    }

    //field
    $product_ads_image_left         = get_field('product_ads_image_left', 'option');
    $product_ads_image_left_link    = get_field('product_ads_image_left_link', 'option');

    $product_ads_image_right        = get_field('product_ads_image_right', 'option');
    $product_ads_image_right_link   = get_field('product_ads_image_right_link', 'option');
?>

<!-- <?php do_action( 'woocommerce_before_main_content' ); ?> -->

<section class="vk-content">

    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="vk-shop__before pt-10 pb-20">
        <div class="container">
            <div class="vk-shop__before-content">
                <div class="_wrapper">
                    <h1 class="vk-shop__heading"><?php echo $term_name; ?></h1>

                    <?php
                        if($count > 0) {
                            echo '<ul class="_list">';
                            foreach ($term_childs as $foreach_kq) {
                                $term_link = get_term_link(get_term($foreach_kq));
                                $term_name = get_term($foreach_kq)->name;
                    ?>
                                <li>
                                    <a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
                                        <?php echo $term_name; ?>
                                    </a>
                                </li>
                    <?php
                            }
                            echo '</ul>';
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>

<!--     <div class="vk-shop__top">
        <div class="container">
            <div class="vk-shop__top-content">

                <div>Tìm kiếm sản phẩm theo</div>
                <aside class="sidebar-filter-product"> -->
                    <?php
                        // dynamic_sidebar('filter-product');
                    ?>
<!--                 </aside>

            </div>
        </div>
    </div> -->

    <div class="vk-shop__mid">
        <div class="container">

            <?php
                if ( woocommerce_product_loop() ) {
                    do_action( 'woocommerce_before_shop_loop' );
                        woocommerce_product_loop_start();
            ?>

                    <div class="vk-shop__list row">

                    <?php
                        if ( wc_get_loop_prop( 'total' ) ) {
                            while ( have_posts() ) {
                                the_post();
                                do_action( 'woocommerce_shop_loop' );

                                // wc_get_template_part( 'content', 'product' );

                                $post_id        = get_the_ID();
                                $post_title     = get_the_title($post_id);
                                $post_date      = get_the_date('d m Y',$post_id);
                                $post_link      = get_permalink($post_id);
                                $post_image     = getPostImage($post_id,"p-product");
                                $post_excerpt   = cut_string(get_the_excerpt($post_id),200,'...');

                                // wc
                                $money =  wc_get_product( $post_id );
                                $old_price = (float)$money->get_regular_price();
                                $price = (float)$money->get_sale_price();

                                $product_sku = $product->get_sku();
                                $product_brand = $product->get_attribute( $attribute_name );

                                //gallery
                                $product = new WC_product($post_id);
                                $single_product_gallery = $product->get_gallery_image_ids();

                                // field
                                $s_p_skill_content = wpautop( get_field('s_p_skill_content') );
                    ?>
                        
                        <div class="col-sm-6 col-md-4 col-lg-3 col-xl-self _item hover-product" data-productid="<?php echo $post_id; ?>">
                            <div class="vk-shop-item ">

                                <?php if( !empty($price) ) { ?>
                                    <div class="vk-shop-item__sale">
                                        - <?php echo show_sale($old_price, $price); ?>
                                    </div>
                                <?php } ?>

                                <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="vk-shop-item__img">
                                    <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
                                </a>
                                <div class="vk-shop-item__brief">
                                    <h3 class="vk-shop-item__title">
                                        <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" data-truncate-lines="1">
                                            <?php echo $post_title; ?>
                                        </a>
                                    </h3>

                                    <div class="vk-shop-item__price">
                                        <?php echo show_price_old_price($old_price,$price,get_woocommerce_currency_symbol()); ?>
                                    </div>

                                    <div class="vk-shop-item__rate">
                                        <?php get_template_part("woocommerce/single-product/rating"); ?>
                                    </div>
                                    <div class="vk-shop-item__hover">
                                        <div class="vk-shop-item__button">
                                            <a title href="javascript:void(0)" class="vk-shop-item__btn">
                                                <?php if($old_price > 0){ ?>
                                                    <form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $post_link ) ); ?>" method="post" enctype='multipart/form-data'>
                                                        <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $post_id ); ?>" class="single_add_to_cart_button button alt">
                                                            <i class="ti-shopping-cart"></i>
                                                        </button>
                                                    </form>
                                                <?php } else { ?>
                                                    <i class="ti-shopping-cart"></i>
                                                <?php } ?>
                                            </a>
                                            <a title href="#quickview_2" data-toggle="modal" class="vk-shop-item__btn">Xem thêm</a>
                                            <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php
                            }
                        }
                    ?>

                    </div>

                    <?php
                        woocommerce_product_loop_end();

                    do_action( 'woocommerce_after_shop_loop' );
                } else {
                    do_action( 'woocommerce_no_products_found' );
                }
            ?>

            <?php
            if(wc_get_loop_prop( 'total_pages' ) > 1) {
                $total   = isset( $total ) ? $total : wc_get_loop_prop( 'total_pages' ); //tong so page
                $current = isset( $current ) ? $current : wc_get_loop_prop( 'current_page' ); //page hien tai
                $base    = isset( $base ) ? $base : esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ); //link
                $format  = isset( $format ) ? $format : '';

                if ( $total <= 1 ) {
                    return;
                }
                ?>
                <nav class="vk-pagination">
                    <?php
                    echo paginate_links(
                        apply_filters(
                            'woocommerce_pagination_args',
                            array(
                                'base'      => $base,
                                'format'    => $format,
                                'add_args'  => false,
                                'current'   => max( 1, $current ),
                                'total'     => $total,
                                'prev_text' => '«',
                                'next_text' => '»',
                                'type'      => 'list',
                                'end_size'  => 1,
                                'mid_size'  => 1,
                            )
                        )
                    );
                    ?>
                </nav>
            <?php } ?>

            <div class="container pt-40">
                <div class="row">
                    <div class="col-lg-6">
                        <a title href="<?php echo $product_ads_image_left_link; ?>" class="vk-img">
                            <img src="<?php echo $product_ads_image_left; ?>" class="_img"  alt="img_ads">
                        </a>
                    </div>
                    <div class="col-lg-6">
                        <a title href="<?php echo $product_ads_image_right_link; ?>" class="vk-img">
                            <img src="<?php echo $product_ads_image_right; ?>" class="_img"  alt="img_ads">
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>





<?php
get_footer( 'shop' );
?>


<style type="text/css">
    .ui-slider .ui-slider-handle {
        transform: none;
    }
    .sidebar-filter-product .widget {
        margin: 10px 0;
    }
    .sidebar-filter-product .widget_price_filter {
        max-width: 300px;
    }
    .sidebar-filter-product .widget_layered_nav ul {
        display: inline-block;
        width: 100%;
    }
    .sidebar-filter-product .widget_layered_nav ul li {
        float: left;
        margin-right: 20px;
    }
</style>