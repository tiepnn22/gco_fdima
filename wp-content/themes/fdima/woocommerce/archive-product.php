<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' ); ?>

<?php
	$terms_info = get_terms( 'product_cat', array(
		'parent'=> 0,
	    'hide_empty' => false
	) );
?>

<section class="vk-content">
    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="vk-shop__before pt-10 pb-20">
        <div class="container">
            <div class="vk-shop__before-content">
                <div class="_wrapper">
                    <h1 class="vk-shop__heading">Sản phẩm</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="vk-shop__mid">
        <div class="container">
            <div class="vk-shop__list row">

            	<?php foreach ($terms_info as $terms_info_kq) {
            		$term_id      = $terms_info_kq->term_id;
            		$term_link    = get_term_link(get_term( $term_id ));
            		$term_name    = $terms_info_kq->name;
            		$thumbnail_id = get_term_meta( $term_id, 'thumbnail_id', true );
            		$term_image   = wp_get_attachment_url( $thumbnail_id );
        		?>

	                <div class="col-sm-6 col-md-4 col-lg-3 col-xl-self _item">
	                    <div class="vk-shop-item ">
	                        <a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>" class="vk-shop-item__img">
	                            <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>" class="_img">
	                        </a>
	                        <div class="vk-shop-item__brief">
	                            <h3 class="vk-shop-item__title">
	                            	<a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>" data-truncate-lines="1">
	                            		<?php echo $term_name; ?>
	                            	</a>
	                            </h3>
	                        </div>
	                    </div>
	                </div>

                <?php } ?>

            </div>
        </div>
    </div>
</section>

<?php get_footer( 'shop' ); ?>
