<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>


<?php
	$product_id = get_the_ID();

	$terms = wp_get_object_terms($post->ID, 'product_cat');
	if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
	$term_id = $term->term_id;
	$term_name = $term->name;

    //woocommerce
    $product = new WC_product($product_id);
    $single_product_gallery = $product->get_gallery_image_ids();
    $single_product_tag = $product->get_tags();

	//price
	$money =  wc_get_product($product_id);
	$old_price = (float)$money->get_regular_price();
	$price = (float)$money->get_sale_price();

    if($old_price > 0){
        if($price > 0 || $price != null){
            $show_sale .= '(Tiết kiệm đến '.show_sale($old_price, $price).' | Giá đã bao gồm thuế VAT)';
        } else {
            $show_sale .= '(Giá đã bao gồm thuế VAT)';
        }
    } else{
        $show_sale .= '';
    }

	// info sku
    $product_sku = $product->get_sku();

    // info attribute
    // $array = wc_get_attribute_taxonomies();
    // foreach ($array as $foreach_kq) {
    //     $attribute_id = $foreach_kq->attribute_id;
    //     $attribute_name = $foreach_kq->attribute_name;
    // }
    // $product_brand = $product->get_attribute( $attribute_name );

    //info product
    $single_product_title   = get_the_title($product_id);
    $single_product_content = wpautop(get_the_content($product_id));
    $single_product_date    = get_the_date('d/m/Y', $product_id);
    $single_product_link    = get_permalink($product_id);
    $single_product_image   = getPostImage($product_id,"full");
    $single_product_excerpt = wpautop(get_the_excerpt($product_id));
    $single_recent_author   = get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
    $single_product_author  = $single_recent_author->display_name;

    // field
    $s_p_video_content = wpautop( get_field('s_p_video_content') );
    $s_p_skill_content = wpautop( get_field('s_p_skill_content') );
?>

<section class="vk-content">
    
    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="vk-shop-detail__content pt-20">
        <div class="container">

            <div class="vk-shop-detail__top">
                <div class="row no-gutters">
                    <div class="col-lg-6">
                        <div class="vk-shop-detail__top-left">

                            <div class="_left">
                                <div class="vk-shop-detail__nav--style-1">
                                    <div class="vk-slider--style-2 slick-slider" data-slider="slider-nav">
                                        
                                        <div class="_item">
                                            <div class="vk-sd-nav-item">
                                                <a title data-zoom-id="zoom" href="<?php echo $single_product_image; ?>" data-image="<?php echo $single_product_image; ?>">
                                                    <img src="<?php echo $single_product_image; ?>" alt="gallery" />
                                                </a>
                                            </div>
                                        </div>

                                        <?php if(!empty( $single_product_gallery )) { ?>
                                        <?php foreach( $single_product_gallery as $foreach_kq ){ ?>
                                        <div class="_item">
                                            <div class="vk-sd-nav-item">
                                                <a title data-zoom-id="zoom" href="<?php echo wp_get_attachment_url( $foreach_kq ); ?>" data-image="<?php echo wp_get_attachment_url( $foreach_kq ); ?>">
                                                    <img src="<?php echo wp_get_attachment_url( $foreach_kq ); ?>" alt="gallery" />
                                                </a>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>

                            <div class="_right">
                                <div class="vk-shop-detail__thumbnail">
                                    <a title class="MagicZoom" id="zoom" href="<?php echo $single_product_image; ?>" data-image="<?php echo $single_product_image; ?>">
                                        <img src="<?php echo $single_product_image; ?>" alt="gallery" />
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="vk-shop-detail__brief">
                            <h2 class="vk-shop-detail__title"><?php echo $single_product_title; ?></h2>
                            <div class="vk-shop-detail__meta">
                                <ul class="vk-shop-detail__list-meta">
                                    <li>
                                        <?php get_template_part("woocommerce/single-product/rating"); ?>
                                    </li>
                                    <li>Mã sản phẩm: <?php echo $product_sku; ?></li>
                                    <!-- <li>Hãng : <a href="#"><?php echo $product_brand; ?></a></li> -->
                                </ul>
                            </div>
                            <div class="vk-shop-detail__info">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="vk-shop-detail__price">
                                            <span class="_current">
                                            	<?php echo show_price_old_price($old_price,$price,get_woocommerce_currency_symbol()); ?>
                                            </span>
                                            <span class="_hint">
                                                <?php echo $show_sale; ?>
                                            </span>
                                        </div>
                                        <div class="vk-shop-detail__specify">
                                            <!-- <p class="mb-2"><strong></strong></p> -->
                                            <?php echo $single_product_excerpt; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vk-shop-detail__button">
                                <?php if($old_price > 0){ ?>
                                    <div class="_quantity">
                                        <span>Số lượng</span>
                                    </div>
                                <?php } ?>
                                <?php get_template_part("woocommerce/single-product/add-to-cart/simple"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="vk-shop-detail__bot pt-50 ">
                <div class="vk-shop-detail__content">
                    <div class="vk-shop-detail__nav">
                        <ul>
                            <li><a title href="#" data-scroll-to="#v1">Video</a></li>
                            <li><a title href="#" data-scroll-to="#v2">Thông số kĩ thuật</a></li>
                            <li><a title href="#" data-scroll-to="#v3">Đánh giá - bình luận</a></li>
                        </ul>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <div class="pt-40" id="v1">
                            	<?php echo $s_p_video_content; ?>
                            </div>

                            <div class="pt-40" id="v2">
                                <?php echo $s_p_skill_content; ?>
                            </div>
                            
                            <div class="vk-divider--style-1 mt-40"></div>
                            
                            <div class="vk-shop-detail__after" id="v3">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="vk-comment">
											<?php
												if ( comments_open() || get_comments_number() ) {
													comments_template();
												}
											?>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="vk-shop-detail__hotline pt-30 pt-lg-0">
                                            <div class="_title">CHÚNG TÔI LUÔN SẴN SÀNG ĐỂ GIÚP ĐỠ BẠN
                                            </div>
                                            <img src="<?php echo asset('images/support.png'); ?>" alt="support">
                                            <p class="mb-0">Để được hỗ trợ tốt nhất. Hãy gọi</p>
                                            <p class="mb-0 _hotline">
                                                <a href="tel:<?php echo str_replace(' ','',get_field('h_phone', 'option'));?>" title>
                                                    <?php echo get_field('h_phone', 'option'); ?>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
