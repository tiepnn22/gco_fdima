<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="canonical" href="<?php echo get_page_link_current(); ?>" />

    <link rel="stylesheet" type="text/css" async="async" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/fonts_roboto.css">

    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<script type="text/javascript">
    var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
    var path_dist = '<?php echo get_template_directory_uri(); ?>/dist/';
</script>

    
<?php
    //field
    $h_phone = get_field('h_phone', 'option');
?>

<header class="vk-header">
    
    <div class="vk-header__mid">
        <div class="container">
            <div class="vk-header__mid-content">

                <?php get_template_part("resources/views/logo"); ?>

                <div class="vk-header__mid-right">

                    <?php get_template_part("resources/views/search-form"); ?>

                    <div class="vk-header__hotline">
                        <div class="_icon"><img src="<?php echo asset('images/icon-hotline-1.png'); ?>" alt="icon"></div>
                        <div class="_number">
                            <?php echo $h_phone;?>
                        </div>
                        <a title href="tel:<?php echo str_replace(' ','',$h_phone);?>" class="_link"></a>
                    </div>

                    <a title href="#" class="vk-header__btn vk-header__btn--account">
                        <i class="ti-user"></i>
                    </a>

                    <div class="vk-header__minicart">
                        <?php get_template_part("resources/views/wc-info-cart"); ?>
                    </div>

                    <a title href="#menu" data-menu="#menu" class="vk-header__btn vk-header__btn--menu d-lg-none">
                        <i class="ti-menu"></i>
                    </a>

                </div>
            </div>
        </div>
    </div>

    <div class="vk-header__bot">
        <div class="container">
            <div class="vk-header__bot-content">
                
                <div class="_left">
                    <a title href="#navCat" data-toggle="collapse" class="vk-header__btn vk-header__btn--nav">
                        <i class="_icon ti-menu"></i> Danh mục sản phẩm
                    </a>
                    <div class="collapse" id="navCat">
                        <?php
                            if(function_exists('wp_nav_menu')){
                                $args = array(
                                    'theme_location'    =>  'menu_cat',
                                    'container_class'   =>  'menu-danh-muc-container',
                                    'menu_class'        =>  'vk-menu__nav-list'
                                );
                                wp_nav_menu( $args );
                            }
                        ?>
                    </div>
                </div>

                <div class="_right">
                    <?php get_template_part("resources/views/menu"); ?>
                </div>

            </div>
        </div>
    </div>

</header>