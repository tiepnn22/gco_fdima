<?php get_header(); ?>

<section class="vk-content">

	<div class="vk-breadcrumb">
	    <nav class="container">
	    	<div class="vk-breadcrumb__list">
				<nav class="breadcrumbs wc-breadcrumbs">
				    <?php _e('Tìm kiếm cho', 'text_domain'); ?> : <?php echo '['.$_GET['s'].']'; ?>
				</nav>
			</div>
		</nav>
	</div>

    <div class="vk-shop__before pt-10 pb-20">
        <div class="container">
            <div class="vk-shop__before-content">
                <div class="_wrapper">
                    <h1 class="vk-shop__heading">Sản phẩm</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="vk-shop__mid">
        <div class="container">

            <div class="vk-shop__list row">

            <?php
				$query = query_search_post_paged( $_GET['s'], array('product'), -1 );
				if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

                    $post_id        = get_the_ID();
                    $post_title     = get_the_title($post_id);
                    $post_date      = get_the_date('d m Y',$post_id);
                    $post_link      = get_permalink($post_id);
                    $post_image     = getPostImage($post_id,"p-product");
                    $post_excerpt   = cut_string(get_the_excerpt($post_id),200,'...');

                    // wc
                    $money =  wc_get_product( $post_id );
                    $old_price = (float)$money->get_regular_price();
                    $price = (float)$money->get_sale_price();

                    $product_sku = $product->get_sku();
                    $product_brand = $product->get_attribute( $attribute_name );

                    //gallery
                    $product = new WC_product($post_id);
                    $single_product_gallery = $product->get_gallery_image_ids();

                    // field
                    $s_p_skill_content = wpautop( get_field('s_p_skill_content') );
            ?>
                
                <div class="col-sm-6 col-md-4 col-lg-3 col-xl-self _item hover-product" data-productid="<?php echo $post_id; ?>">
                    <div class="vk-shop-item ">

                        <?php if( !empty($price) ) { ?>
                            <div class="vk-shop-item__sale">
                                - <?php echo show_sale($old_price, $price); ?>
                            </div>
                        <?php } ?>

                        <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="vk-shop-item__img">
                            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
                        </a>
                        <div class="vk-shop-item__brief">
                            <h3 class="vk-shop-item__title">
                                <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" data-truncate-lines="1">
                                    <?php echo $post_title; ?>
                                </a>
                            </h3>

                            <div class="vk-shop-item__price">
                                <?php echo show_price_old_price($old_price,$price,get_woocommerce_currency_symbol()); ?>
                            </div>

                            <div class="vk-shop-item__rate woocommerce">
                                <?php get_template_part("woocommerce/single-product/rating"); ?>
                            </div>
                            <div class="vk-shop-item__hover">
                                <div class="vk-shop-item__button">
                                    <a title href="javascript:void(0)" class="vk-shop-item__btn woocommerce">
                                        <?php if($old_price > 0){ ?>
                                            <form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $post_link ) ); ?>" method="post" enctype='multipart/form-data'>
                                                <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $post_id ); ?>" class="single_add_to_cart_button button alt">
                                                    <i class="ti-shopping-cart"></i>
                                                </button>
                                            </form>
                                        <?php } else { ?>
                                            <i class="ti-shopping-cart"></i>
                                        <?php } ?>
                                    </a>
                                    <a title href="#quickview_2" data-toggle="modal" class="vk-shop-item__btn">Xem thêm</a>
                                    <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

            </div>

        </div>
    </div>
</section>

<section class="vk-content">
    <div class="vk-shop__before pt-10 pb-20">
        <div class="container">
            <div class="vk-shop__before-content">
                <div class="_wrapper">
                    <h1 class="vk-shop__heading">Tin tức</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="vk-blog__list row">

			<?php
				$query = query_search_post_paged( $_GET['s'], array('post'), -1 );
				if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

                $post_id        = get_the_ID();
                $post_title     = get_the_title($post_id);
                $post_content   = wpautop(get_the_content($post_id));
                $post_date      = 'Ngày '.get_the_date('d / m / Y',$post_id);
                $post_link      = get_permalink($post_id);
                $post_image     = getPostImage($post_id,"p-post");
                $post_excerpt   = cut_string(get_the_excerpt($post_id),135,'...');
                $post_author    = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                $post_tag       = get_the_tags($post_id);
			?>

	            <div class="col-sm-6 col-md-3 col-lg-4 _item">
	                <div class="vk-blog-item ">
	                    <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="vk-blog-item__img">
	                        <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
	                    </a>
	                    <div class="vk-blog-item__brief">
	                        <h3 class="vk-blog-item__title">
	                        	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
	                        		<?php echo $post_title; ?>
	                        	</a>
	                        </h3>
	                        <div class="vk-blog-item__date"><?php echo $post_date; ?></div>
	                        <div class="vk-blog-item__text" data-truncate-lines="2">
	                            <?php echo $post_excerpt; ?>
	                        </div>
	                    </div>
	                </div>
	            </div>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>