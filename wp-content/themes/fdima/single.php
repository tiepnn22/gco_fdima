<?php get_header(); ?>

<?php
	$post_id = get_the_ID();

	$get_category = get_the_category($post_id);
	foreach ( $get_category as $get_category_kq ) {
	    $cat_id   = $get_category_kq->term_id;
	}
	$cat_name = get_cat_name($cat_id);
	
	//info post
	$single_post_title 		= get_the_title($post_id);
	$single_post_content 	= wpautop(get_the_content($post_id));
	$single_post_date 		= 'Ngày '.get_the_date('d / m / Y',$post_id);
	$single_post_link 		= get_permalink($post_id);
    $single_post_image 		= getPostImage($post_id,"p-post");
	$single_post_excerpt 	= cut_string(get_the_excerpt($post_id),300,'...');
	$single_recent_author 	= get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
	$single_post_author 	= $single_recent_author->display_name;
    $single_post_tag 		= get_the_tags($post_id);
?>

<section class="vk-content">
    
    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="container">
        <div class="vk-blog-detail__top">
            <div class="vk-blog-detail__wrapper">
                <div class="vk-blog-detail__box-title">
                    <h1 class="vk-blog-detail__title">
                        <?php echo $single_post_title; ?>
                    </h1>
                    <div class="vk-blog-detail__date">
                        <?php echo $single_post_date; ?>
                    </div>
                </div>
                <div class="vk-blog-detail__content">
                    <?php echo $single_post_content; ?>
                </div>
            </div>
        </div>
        
		<?php get_template_part("resources/views/template-related-post"); ?>
		
    </div>
</section>

<?php get_footer(); ?>