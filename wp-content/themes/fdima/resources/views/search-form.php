<div class="vk-header__search">

	<form class="vk-form vk-form--search" action="<?php echo esc_url( home_url( '/' ) ); ?>">

        <input type="text" class="form-control" required="required" placeholder="Tìm kiếm..." name="s" value="<?php echo get_search_query(); ?>">
        <button type="submit" class="vk-btn">
        	<i class="ti-search"></i>
        </button>

    </form>

</div>