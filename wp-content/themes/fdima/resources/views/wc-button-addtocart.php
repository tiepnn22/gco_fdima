<?php
	global $data_button_addtocart;
	
	if(!empty( $data_show_price )) {
		$post_id 	= $data_button_addtocart['post_id'];
		$post_link  = $data_button_addtocart['post_link'];
	}

	$post_id   == '' ? get_the_ID() : $post_id;
	$post_link == '' ? get_permalink($post_id) : $post_link;
?>

<div class="product__control" style="transform: translateY(0);">
	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $post_link ) ); ?>" method="post" enctype='multipart/form-data'>
		<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $post_id ); ?>" class="single_add_to_cart_button button alt">
			<?php _e('Mua hàng', 'text_domain'); ?>
		</button>
	</form>
</div>