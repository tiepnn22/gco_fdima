<?php
	//$glb_ctp_product biến toàn cục
	//$glb_tax_product biến toàn cục
	global $post;
	$terms 		= get_the_terms( $post->ID , $glb_tax_product, 'string');
	$term_ids 	= wp_list_pluck($terms,'term_id');
	
	$query = new WP_Query( array(
		'post_type' 	 => $glb_ctp_product,
		'tax_query' 	 => array(
			array(
				'taxonomy' 	=> $glb_tax_product,
				'field' 	=> 'id',
				'terms' 	=> $term_ids,
				'operator'	=> 'IN'
			 )),
		'posts_per_page' => 4,
		'orderby' 		 => 'date',
		'post__not_in'	 => array($post->ID)
	) );
?>


<aside class="related-product">
	<div class="container">
		<div class="bao-while">

			<div class="related-title">
				<h3>
					<?php _e('Sản phẩm liên quan', 'text_domain'); ?>
				</h3>
			</div>
			<div class="related-product-content">
				<div class="row">
					
					<?php
						if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

						$post_id 		= get_the_ID();
	            		$post_title 	= get_the_title($post_id);
	            		$post_content 	= wpautop(get_the_content($post_id));
	            		$post_date 		= get_the_date('Y/m/d',$post_id);
	            		$post_link 		= get_permalink($post_id);
	            		$post_image 	= getPostImage($post_id,"product");
	            		$post_excerpt 	= cut_string(get_the_excerpt($post_id),300,'...');
	            		$post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	            		$post_tag 		= get_the_tags($post_id);
					?>

						<div class="title">
					        <a href="<?php the_permalink(); ?>" title="<?php echo $post_title; ?>" rel="bookmark">
					      	    <h4><?php the_title(); ?></h4>
					        </a>
					    </div>

					<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

				</div>
			</div>

		</div>
	</div>
</aside>

