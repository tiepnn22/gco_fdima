<?php
    //field
    $h_logo = get_field('h_logo', 'option');
?>

<div class="vk-header__mid-left">

    <a href="<?php echo get_option('home');?>" class="vk-header__logo" 
    	title="<?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?>">

        <img src="<?php echo $h_logo; ?>" 
        alt="<?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?>">

    </a>
    
</div>