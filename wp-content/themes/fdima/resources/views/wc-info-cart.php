<?php
    //cart
    $info_cart     = WC()->cart->cart_contents;
    $cart_count    = WC()->cart->get_cart_contents_count();
    $cart_total    = WC()->cart->get_cart_total();
    $cart_page_url = wc_get_cart_url();

    //checkout
    $checkout_page_url = wc_get_checkout_url();
?>

<a title href="<?php echo $cart_page_url; ?>" class="vk-header__btn vk-header__btn--minicart">
    <i class="ti-shopping-cart"></i>
</a>

<div class="vk-minicart">

    <?php if($cart_count > 0) { ?>
        <div class="vk-minicart__list">

        <?php
            foreach ($info_cart as $info_cart_kq) {
                
            $product_id         = $info_cart_kq["product_id"];
            $product_title      = get_the_title($product_id);
            $product_link       = get_permalink($product_id);
            $product_image      = getPostImage($product_id,"p-product");

            $money              =  wc_get_product($product_id);
            $oldprice           = (float)$money->get_regular_price();
            $price              = (float)$money->get_sale_price();

            $product_quantity   = $info_cart_kq["quantity"];
        ?>

            <div class="vk-minicart-item">
                <a href="<?php echo $product_link; ?>" class="vk-minicart-item__img" title="<?php echo $product_title; ?>">
                    <img src="<?php echo $product_image; ?>" alt="<?php echo $product_title; ?>">
                </a>
                <div class="vk-minicart-item__brief">
                    <h3 class="vk-minicart-item__title"><?php echo $product_title; ?></h3>
                    <div class="vk-minicat-item__text" style="display: flex; justify-content: space-between;">
                        <span><?php echo show_price_old_price($oldprice,$price,get_woocommerce_currency_symbol()); ?></span>
                        <span><?php echo $product_quantity; ?></span>
                    </div>
                </div>
            </div>

        <?php } ?>

        </div>

        <div class="vk-minicart__total">
            <span>Tổng:</span>
            <span><?php echo $cart_total; ?></span>
        </div>
        <div class="vk-minicart__button">
            <a title href="<?php echo $cart_page_url; ?>" class="vk-minicart__btn">Giỏ hàng</a>
            <a title href="<?php echo $checkout_page_url; ?>" class="vk-minicart__btn">Thanh toán</a>
        </div>
    <?php } else { ?>
        <?php _e('Giỏ hàng hiện đang trống.', 'text_domain'); ?>
    <?php } ?>
    
</div>