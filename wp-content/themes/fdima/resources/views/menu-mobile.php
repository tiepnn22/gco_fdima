<?php
    if(function_exists('wp_nav_menu')){
        $args = array(
            'theme_location' 	=> 	'mobile',
            'container_class'	=>	'menu-menu-mobile-container',
            'menu_class'		=>	''
        );
        wp_nav_menu( $args );
    }
?>