<?php
    //field
    $f_logo = get_field('f_logo', 'option');
?>

<a href="<?php echo get_option('home');?>" class="vk-footer__logo" 
	title="<?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?>">

    <img src="<?php echo $f_logo; ?>" 
    alt="<?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?>">

</a>
