<?php
    global $post;
    $categories = get_the_category($post->ID);

    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args=array(
	    'category__in'         => $category_ids,
	    'post__not_in'         => array($post->ID),
	    'posts_per_page'       => 3,
	    'ignore_sticky_posts'  =>1
    );
    $query = new wp_query( $args );
    $post_count = $query->post_count;
?>

<div class="vk-blog-detail__relate">
    <h2 class="vk-blog-detail__title mb-0 pb-20">Tin tức liên quan</h2>

    <!-- <div class="vk-blog__list row slick-slider" data-slider="blog"> -->
    <div class="vk-blog__list row <?php if($post_count>5){echo 'slick-slider';} ?>" <?php if($post_count>5){echo 'data-slider="blog"';} ?>>
        
        <?php
            if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

            $post_id            = get_the_ID();
            $post_title         = get_the_title($post_id);
            $post_content       = wpautop(get_the_content($post_id));
            $post_date          = 'Ngày '.get_the_date('d / m / Y',$post_id);
            $post_link          = get_permalink($post_id);
            $post_image         = getPostImage($post_id,"p-post");
            $post_excerpt       = cut_string(get_the_excerpt($post_id),300,'...');
            $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
            $post_tag           = get_the_tags($post_id);
            $post_comment       = wp_count_comments($post_id);
            $post_comment_total = $post_comment->total_comments;
        ?>

            <div class="col-sm-6 col-md-3 col-lg-4 _item">
                <div class="vk-blog-item ">
                    <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="vk-blog-item__img">
                        <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="_img">
                    </a>
                    <div class="vk-blog-item__brief">
                        <h3 class="vk-blog-item__title">
                            <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                                <?php echo $post_title; ?>
                            </a>
                        </h3>
                        <div class="vk-blog-item__date"><?php echo $post_date; ?></div>
                        <div class="vk-blog-item__text" data-truncate-lines="2">
                            <?php echo $post_excerpt; ?>
                        </div>
                    </div>
                </div>
            </div>

        <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

    </div>
</div>