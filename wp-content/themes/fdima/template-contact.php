<?php
	/*
	Template Name: Mẫu Liên hệ
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //field
    $contact_contact_title  = get_field('contact_contact_title');
    $contact_contact_form   = get_field('contact_contact_form');
    $contact_map            = get_field('contact_map');
?>

<section class="vk-content">

    <?php get_template_part("resources/views/page-banner"); ?>

    <div class="vk-shop__before pt-20 pb-20">
        <div class="container">
            <div class="vk-shop__before-content">
                <h1 class="vk-shop__heading"><?php echo $contact_contact_title; ?></h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="vk-contact__form">
            <div class="vk-form vk-form--contact">

                <?php if(!empty( $contact_contact_form )) { ?>
                    <?php echo do_shortcode($contact_contact_form); ?>
                <?php } ?>

            </div>
        </div>
    </div>

    <div class="vk-contact__map pt-50">
        <?php echo $contact_map; ?>
    </div>
</section>

<?php get_footer(); ?>