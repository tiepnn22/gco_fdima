<?php
    //field
	$f_partner_title         = get_field('f_partner_title', 'option');
	$f_partner_gallery       = get_field('f_partner_gallery', 'option');

	$f_top_subscribe_title   = get_field('f_top_subscribe_title', 'option');
	$f_top_subscribe_form    = get_field('f_top_subscribe_form', 'option');

    $f_socical_facebook      = get_field('f_socical_facebook', 'option');
    $f_socical_twiter        = get_field('f_socical_twiter', 'option');
    $f_socical_linkin        = get_field('f_socical_linkin', 'option');
    $f_socical_skype         = get_field('f_socical_skype', 'option');

    $f_title_section_1       = get_field('f_title_section_1', 'option');
    $f_select_url_1          = get_field('f_select_url_1', 'option');

    $f_title_section_2       = get_field('f_title_section_2', 'option');
    $f_select_url_2          = get_field('f_select_url_2', 'option');

    $f_title_section_3       = get_field('f_title_section_3', 'option');
    $f_select_url_3          = get_field('f_select_url_3', 'option');

    $f_title_section_4       = get_field('f_title_section_4', 'option');
    $f_phone                 = get_field('f_phone', 'option');
    $f_email                 = get_field('f_email', 'option');

    $f_bottom_left_copyright = get_field('f_bottom_left_copyright', 'option');
    $f_bottom_left_card      = get_field('f_bottom_left_card', 'option');

    $f_bottom_right_gallery  = get_field('f_bottom_right_gallery', 'option');
?>

<footer class="vk-footer">

    <?php
        if(!empty( $f_partner_gallery )) {
            $i = 0;
            foreach ($f_partner_gallery as $foreach_kq) {
                
            $i++; }
    ?>
    <div class="vk-partner">
        <div class="container">
            <div class="vk-partner__content">
                <h2 class="vk-partner__heading"><?php echo $f_partner_title; ?></h2>
                <!-- <div class="vk-slider--style-1 slick-slider row" data-slider="partner"> -->
                <div class="vk-slider--style-1 row <?php if($i>6){echo 'slick-slider';} ?>" <?php if($i>6){echo 'data-slider="partner"';} ?>>

                    <?php
                        foreach ($f_partner_gallery as $foreach_kq) {

                        $post_image = $foreach_kq;
                    ?>
                        <div class="_item col-lg-2">
                            <div class="vk-partner-item">
                                <div class="vk-partner-item__img">
                                    <img src="<?php echo $post_image; ?>" alt="partner">
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <?php } ?>

    <div class="vk-newsregister">
        <div class="container">
            <div class="vk-newsregister__content">
                <span class="vk-newsregister__title"><?php echo $f_top_subscribe_title; ?></span>

                <?php if(!empty( $f_top_subscribe_form )) { ?>
                <div class="vk-form vk-form--newsregister">
                    <?php echo do_shortcode($f_top_subscribe_form); ?>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>

    <div class="container">

        <div class="vk-footer__top">
            <div class="vk-footer__top-content">
                <div class="_left">
					<?php get_template_part("resources/views/logo-footer"); ?>
                </div>
                <div class="_right">
                    <ul class="vk-footer__list vk-footer__list--style-1">
                        <li>
                        	<a title href="<?php echo $f_socical_facebook; ?>" target="_blank">
                        		<i class="fa fa-facebook"></i>
                        	</a>
                        </li>
                        <li>
                        	<a title href="<?php echo $f_socical_twiter; ?>" target="_blank">
                        		<i class="fa fa-twitter"></i>
                        	</a>
                        </li>
                        <li>
                        	<a title href="<?php echo $f_socical_linkin; ?>" target="_blank">
                        		<i class="fa fa-linkedin"></i>
                        	</a>
                        </li>
                        <li>
                        	<a title href="<?php echo $f_socical_skype; ?>" target="_blank">
                        		<i class="fa fa-skype"></i>
                        	</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="vk-footer__mid">
            <div class="vk-footer__mid-content row">

                <div class="col-lg-3">
                    <div class="vk-footer__item">
                        <h2 class="vk-footer__title"><?php echo $f_title_section_1; ?></h2>
						
						<?php if(!empty( $f_select_url_1 )) { ?>
                        <ul class="vk-footer__list vk-footer__list--style-3">

							<?php
							    foreach ($f_select_url_1 as $foreach_kq) {

							    $post_title   = $foreach_kq["link"]["title"];
							    $post_link    = $foreach_kq["link"]["url"];
                                $post_target  = $foreach_kq["link"]["target"];
							?>
								<li>
								    <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" target="<?php echo $post_target; ?>">
								        <?php echo $post_title; ?>
								    </a>
								</li>
							<?php } ?>

                        </ul>
						<?php } ?>

                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="vk-footer__item">
                        <h2 class="vk-footer__title"><?php echo $f_title_section_2; ?></h2>

						<?php if(!empty( $f_select_url_2 )) { ?>
                        <ul class="vk-footer__list vk-footer__list--style-3">

							<?php
							    foreach ($f_select_url_2 as $foreach_kq) {

							    $post_title   = $foreach_kq["link"]["title"];
							    $post_link    = $foreach_kq["link"]["url"];
                                $post_target  = $foreach_kq["link"]["target"];
							?>
								<li>
								    <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" target="<?php echo $post_target; ?>">
								        <?php echo $post_title; ?>
								    </a>
								</li>
							<?php } ?>
							
                        </ul>
						<?php } ?>

                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="vk-footer__item">
                        <h2 class="vk-footer__title"><?php echo $f_title_section_3; ?></h2>

						<?php if(!empty( $f_select_url_3 )) { ?>
                        <ul class="vk-footer__list vk-footer__list--style-3">

							<?php
							    foreach ($f_select_url_3 as $foreach_kq) {

							    $post_title   = $foreach_kq["link"]["title"];
							    $post_link    = $foreach_kq["link"]["url"];
                                $post_target  = $foreach_kq["link"]["target"];
							?>
								<li>
								    <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" target="<?php echo $post_target; ?>">
								        <?php echo $post_title; ?>
								    </a>
								</li>
							<?php } ?>
							
                        </ul>
						<?php } ?>

                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="vk-footer__item">
                        <h2 class="vk-footer__title"><?php echo $f_title_section_4; ?></h2>
                        <div>Hotline trực tuyến</div>
                        <div>
                        	<a title href="tel:<?php echo str_replace(' ','',$f_phone);?>" class="vk-footer__hotline">
                        		<?php echo $f_phone; ?>
                    		</a>
                        </div>
                        <div>Email: <a title href="mailto:<?php echo $f_email; ?>"><?php echo $f_email; ?></a></div>
                    </div>
                </div>

            </div>

            <div class="vk-footer__bot">
                <div class="_left">
                    <span>
                        <?php echo $f_bottom_left_copyright; ?>
                    </span>
                    <img src="<?php echo $f_bottom_left_card; ?>" alt="copyright">
                </div>
                <div class="_right">

					<?php if(!empty( $f_bottom_right_gallery )) { ?>
					<?php
					    foreach ($f_bottom_right_gallery as $foreach_kq) {

					    $post_image = $foreach_kq;
					?>
						<img src="<?php echo $post_image; ?>" alt="bocongthuong">
					<?php } ?>
					<?php } ?>

                </div>
            </div>
        </div>

    </div>
</footer>


<div class="modal fade" id="quickview_2" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="vk-shop-detail__top">
                    <div class="row no-gutters">
                        <div class="col-lg-6">
                            <div class="vk-shop-detail__top-left">

                                <div class="_left">
                                    <div class="vk-shop-detail__nav--style-1">
                                        <div class="vk-slider--style-2 slick-slider" data-slider="slider-nav">
                                            <div class="_item">
                                                <div class="vk-sd-nav-item">
                                                    <a data-zoom-id="zoom" href="" data-image="" title="...">
                                                        <img src="" />
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="_item">
                                                <div class="vk-sd-nav-item">
                                                    <a data-zoom-id="zoom" href="" data-image="" title="...">
                                                        <img src="" />
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="_right">
                                    <div class="vk-shop-detail__thumbnail">
                                        <a class="MagicZoom" id="zoom" href="" data-image="" title="...">
                                            <img src="" />
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="vk-shop-detail__brief">
                                <h2 class="vk-shop-detail__title"></h2>
                                <div class="vk-shop-detail__meta">
                                    <ul class="vk-shop-detail__list-meta woocommerce">
                                        <li>
                                            
                                        </li>
                                        <li>Mã sản phẩm: </li>
                                        <!-- <li>Hãng : <a href="#"></a></li> -->
                                    </ul>
                                </div>
                                <div class="vk-shop-detail__info">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="vk-shop-detail__price">
                                                <span class="_current">
                                                    
                                                </span>
                                                <span class="_hint">(Tiết kiệm đến  | Giá đã bao gồm thuế VAT)</span>
                                            </div>
                                            <div class="vk-shop-detail__specify">
                                                <p class="mb-2"><strong></strong></p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vk-shop-detail__button">
                                    <div class="_quantity">
                                        <span>Số lượng</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<nav class="" id="menu">
	<?php get_template_part("resources/views/menu-mobile"); ?>
</nav>


<?php wp_footer(); ?>

</body>
</html>