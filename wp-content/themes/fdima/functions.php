<?php
include_once get_template_directory(). '/load/Custom_Functions.php';
// include_once get_template_directory(). '/load/CTPost_CTTax.php';
include_once get_template_directory(). '/load/Performance.php';
include_once get_template_directory(). '/load/wc.php';


/* Create CTPost */
// (title, slug_code, slug)
// create_post_type("Sản phẩm","product","sanpham");
/* Create CTTax */
// (title, slug, slug_code, post_type)
// create_taxonomy_theme("Danh mục Sản phẩm","danhmuc-sanpham","product-cat","product");


// Create menu Theme option use Acf Pro
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Tuỳ chỉnh', // Title hiển thị khi truy cập vào Options page
        'menu_title'    => 'Tuỳ chỉnh', // Tên menu hiển thị ở khu vực admin
        'menu_slug'     => 'theme-settings', // Url hiển thị trên đường dẫn của options page
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}


// Title Head Page
if (!function_exists('title')) {
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_option('blogname') .' - '. get_option('blogdescription');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            $return = $obj->name;
                if($return == 'product'){
                    $return_kq = 'Sản phẩm';
                }else{
                    $return_kq = $return;
                }
            return $return_kq;
        }

        if (is_search()) {
            return __( 'Tìm kiếm cho', 'text_domain' ).' : ['.$_GET['s'].']';
        }

        if (is_404()) {
            return __( '404 Không tìm thấy trang', 'text_domain' );
        }

        return get_the_title();
    }
}


// Url File theme
if (!function_exists('asset')) {
    function asset($path)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $path);
    }
}


// Url image theme
if (!function_exists('getPostImage')) {
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? asset('images/no-image-wc.png') : $img[0];
    }
}


// Cut String text
if (!function_exists('cut_string')) {
    function cut_string($str,$len,$more){
        if ($str=="" || $str==NULL) return $str;
        if (is_array($str)) return $str;
            $str = trim(strip_tags($str));
        if (strlen($str) <= $len) return $str;
            $str = substr($str,0,$len);
        if ($str != "") {
            if (!substr_count($str," ")) {
              if ($more) $str .= " ...";
              return $str;
            }
            while(strlen($str) && ($str[strlen($str)-1] != " ")) {
                $str = substr($str,0,-1);
            }
            $str = substr($str,0,-1);
            if ($more) $str .= " ...";
        }
        return $str;
    }
}


// Get url page here
if (!function_exists('get_page_link_current')) {
    function get_page_link_current(){
        // Get url page here
        $page_link_current_get = sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['REQUEST_URI']
        );

        // Get url, exclude url GET
        $page_link_current_strstr = strstr($page_link_current_get, '?');
        $page_link_current        = str_replace( $page_link_current_strstr, '', $page_link_current_get );

        return $page_link_current;
    }
}


// Get url page template by name file (vd : template-contact.php)
if (!function_exists('get_link_page_template')) {
    function get_link_page_template($name_file){
        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => $name_file
        ));
        
        $page_template_link = get_page_link($pages[0]->ID);

        return $page_template_link;
    }
}


// Check Language
if (!function_exists('get_data_language')) {
    function get_data_language($dataVN, $dataEN) {
        if(ICL_LANGUAGE_CODE == 'vi'){
            $get_data_language = $dataVN;
        } elseif (ICL_LANGUAGE_CODE == 'en') {
            $get_data_language = $dataEN;
        }
        return $get_data_language;
    }
}


// Format price
if (!function_exists('format_price')) {
    function format_price($money) {
        $str = "";
        if ($money != 0) {
            $num = (float)$money;
            $str = number_format($num,0,'.','.');
        }
        return $str;
    }
}
// Format price + VNĐ
if (!function_exists('format_price_donvi')) {
    function format_price_donvi($money, $donvi) {
        $str = "";
        if($money != 0) {
            $num = (float)$money;
            $str = number_format($num,0,'.','.');
            $str .= ' '.$donvi;
            $str = $str;
        }
        return $str;
    }
}
// Check old_price, price + VNĐ
if (!function_exists('show_price_old_price')) {
    function show_price_old_price($old_price, $price, $donvi2) {
        $donvi = !empty($donvi2) ? $donvi2 : '';
        $str = "";
        if($old_price > 0){
            if($price > 0 || $price != null){
                $str1 = format_price_donvi($price, $donvi);
                $str2 = format_price_donvi($old_price, $donvi);
                $str = '<div class="price">
                            <span class="_current">'.$str1.'</span>
                        </div>';
            } else {
                $str = '<div class="price">
                            <span class="_current">'.format_price_donvi($old_price, $donvi).'</span>
                        </div>';
            }
        } else{
            $str = '<div class="price">
                        <a class="_current" href="'.get_link_page_template('template-contact.php').'">Liên hệ</a>
                    </div>';
        }
        return $str;
    }
}
// Show % sale price
if (!function_exists('show_sale')) {
    function show_sale($old_price, $price) {
        if($old_price == 0){ } else {
            if($price == 0){ } else {
                $sale = (1 - ($price / $old_price))*100;
                $show_sale = '<span class="price-sale">'.ceil($sale).'%</span>';
            }
        }
        return $show_sale;
    }
}


// Pagination
function paginationCustom($max_num_pages) {
    echo '<ul class="vk-list vk-list--inline vk-pagination__list">';
    if ($max_num_pages > 1) {   // tổng số trang (10)
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; // trang hiện tại (8)

        if ($max_num_pages > 9) {
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( 1 ) ).'" class="item">
            ...</a></li>';
        }
        if ($paged > 1) {
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( $paged - 1 ) ).'" class="item">
            <i class="fa fa-angle-left"></i></a></li>';
        }
        if ($paged >= 5 && $max_num_pages > 9) {
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( $paged - 5 ) ).'" class="item">
            -5</a></li>';
            echo '<li class="vk-list__item"><a href="javascript:void(0)" class="">&nbsp;</a></li>';
        }

        for($i= 1; $i <= $max_num_pages; $i++) {
            // $half_total_links = floor( 5 / 2);
            $half_total_links = 2;

            $from = $paged - $half_total_links; // trang hiện tại - 2 (8-2= 6)
            $to = $paged + $half_total_links;   // trang hiện tại + 2 (8+2 = 10)

            // if ($paged < $half_total_links) {
            //    $to += $half_total_links - $paged;
            // }
            // if ($max_num_pages - $paged < $half_total_links) {
            //     $from -= $half_total_links - ($max_num_pages - $paged) - 1;
            // }

            if ($from < $i && $i < $to) {   // $form cách $to 3 số (từ 6 đến 10 là 7,8,9)
                $class = $i == $paged ? 'current' : 'item';
                echo '<li class="vk-list__item"><a class="'.$class.'" href="'.esc_url( get_pagenum_link( $i ) ).'">'.$i.'</a></li>';
            }
        }

        if ($paged <= $max_num_pages - 5 && $max_num_pages > 9) {
            echo '<li class="vk-list__item"><a href="javascript:void(0)" class="">&nbsp;</a></li>';
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( $paged + 5 ) ).'" class="item">
            +5</a></li>';
        }
        if ($paged + 1 <= $max_num_pages) {
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( $paged + 1 ) ).'" class="item">
            <i class="fa fa-angle-right"></i></a></li>';
        }
        if ($max_num_pages > 9) {
            echo '<li class="vk-list__item"><a href="'.esc_url( get_pagenum_link( $max_num_pages ) ).'" class="item">
            ...</a></li>';
        }
    }
    echo '</ul>';
}


// Ajax Readmore_post Product
add_action('wp_ajax_Readmore_post', 'Readmore_post');
add_action('wp_ajax_nopriv_Readmore_post', 'Readmore_post');
function Readmore_post() {
    global $product;

    $data_productid = $_POST['data_productid'];

    $data = array();

    // $data['exit'] = $data_productid;

    $post_id = $data_productid;
    $post_title = get_the_title($post_id);
    $post_date = get_the_date('d m Y',$post_id);
    $post_link = get_permalink($post_id);
    $post_image = getPostImage($post_id,"full");
    $post_excerpt = cut_string(get_the_excerpt($post_id),200,'...');

    // wc
    $money =  wc_get_product( $post_id );
    $old_price = (float)$money->get_regular_price();
    $price = (float)$money->get_sale_price();

    //add_to_cart_button
    if($old_price > 0){
        $add_to_cart_button .= '
        <div class="_quantity">
            <span>Số lượng</span>
        </div>
        <form class="cart" action="'.$post_link.'" method="post" enctype="multipart/form-data">
            <div class="quantity">
                <label class="screen-reader-text" for="quantity_60afce1e20112">Bàn là hơi nước Sokany ES-178-2 quantity</label>
                <input type="number" id="quantity_60afce1e20112" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" placeholder="" inputmode="numeric">
            </div>
            <button type="submit" name="add-to-cart" value="'.$post_id.'" class="single_add_to_cart_button button alt">Mua sản phẩm</button>
        </form>
        ';
    } else {
        $add_to_cart_button .= '';
    }

    // % price, VAT
    if($old_price > 0){
        if($price > 0 || $price != null){
            $show_sale .= '(Tiết kiệm đến '.show_sale($old_price, $price).' | Giá đã bao gồm thuế VAT)';
        } else {
            $show_sale .= '(Giá đã bao gồm thuế VAT)';
        }
    } else{
        $show_sale .= '';
    }


    // product;
    $product = wc_get_product( $post_id );
    foreach ($product->attributes as $key => $foreach_kq) {

    }
    $product_attributes_name = $key;
    $product_sku = $product->sku;
    // $product_brand = array_shift( wc_get_product_terms( $post_id, $product_attributes_name, array( 'fields' => 'names' ) ) );

    // gallery
    $product = new WC_product($post_id);
    $single_product_gallery = $product->get_gallery_image_ids();
    foreach( $single_product_gallery as $foreach_kq ){
        $single_product_gallery_kq .= '
        <div class="_item">
            <div class="vk-sd-nav-item">
                <a data-zoom-id="zoom" href="'.wp_get_attachment_url( $foreach_kq ).'" data-image="'.wp_get_attachment_url( $foreach_kq ).'" title="...">
                    <img src="'.wp_get_attachment_url( $foreach_kq ).'" />
                </a>
            </div>
        </div>
        ';
    }

    //rating
    if ($ave = $product->get_average_rating($post_id)) {

        $ave_check = $ave/5*100;

        $rating_kq .= '
        <div class="woocommerce-product-rating">
            <div class="star-rating" role="img" aria-label="Rated 3.50 out of 5">
                <span style="width:'.$ave_check.'%">
                </span>
            </div>
        </div>';
    } else {
        $rating_kq .= '
        <div class="woocommerce-product-rating">
            <div class="star-rating" role="img" aria-label="Rated 3.50 out of 5">
            </div>
        </div>';
    }

    // field
    $s_p_skill_content = wpautop( get_post_meta( $post_id, 's_p_skill_content', true ) );


    $data['result'] .= '
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
            <div class="vk-shop-detail__top">
                <div class="row no-gutters">
                    <div class="col-lg-6">
                        <div class="vk-shop-detail__top-left">

                            <div class="_left">
                                <div class="vk-shop-detail__nav--style-1">
                                    <div class="vk-slider--style-2 slick-slider" data-slider="slider-nav">
                                        <div class="_item">
                                            <div class="vk-sd-nav-item">
                                                <a data-zoom-id="zoom" href="'.$post_image.'" data-image="'.$post_image.'" title="...">
                                                    <img src="'.$post_image.'" />
                                                </a>
                                            </div>
                                        </div>

                                        '.$single_product_gallery_kq.'

                                    </div>
                                </div>
                            </div>

                            <div class="_right">
                                <div class="vk-shop-detail__thumbnail">
                                    <a class="MagicZoom" id="zoom" href="'.$post_image.'" data-image="'.$post_image.'" title="...">
                                        <img src="'.$post_image.'" />
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="vk-shop-detail__brief">
                            <h2 class="vk-shop-detail__title">'.$post_title.'</h2>
                            <div class="vk-shop-detail__meta">
                                <ul class="vk-shop-detail__list-meta woocommerce">
                                    <li>
                                        '.$rating_kq.'
                                    </li>
                                    <li>Mã sản phẩm: '.$product_sku.'</li>
                                </ul>
                            </div>
                            <div class="vk-shop-detail__info">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="vk-shop-detail__price">
                                            <span class="_current">
                                                '.show_price_old_price($old_price,$price,get_woocommerce_currency_symbol()).'
                                            </span>
                                            <span class="_hint">
                                                '.$show_sale.'
                                            </span>
                                        </div>
                                        <div class="vk-shop-detail__specify">
                                            '.$s_p_skill_content.'
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vk-shop-detail__button woocommerce">
                                '.$add_to_cart_button.'
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="'.get_stylesheet_directory_uri().'/dist/plugin/magiczoomplus/magiczoomplus.js"></script>
    ';

    echo json_encode($data);
    die();
}