<?php get_header(); ?>

<?php
	$page_id       = get_the_ID();
	$page_name     = get_the_title();
	$page_content  = get_the_content();
?>

<section class="vk-content">
    
    <?php get_template_part("resources/views/page-banner"); ?>
    
    <div class="container pt-10">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="vk-blog-detail__content">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>